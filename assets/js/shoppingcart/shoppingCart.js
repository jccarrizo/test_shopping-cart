(function ($) {


    $(".form-items").submit(function (ev) {
        ev.preventDefault();
    });

    $(document).ready(function () {
        $("input[type='number']").inputSpinner();
    });

    $(".update-btn").click(function () {
        var form = $(this).parents('form:first');
        var action_form = form.attr('action');
        form.attr('action', form.attr('action') + "/updateItem");
        var formData = new FormData();
        var btn_update = $(this);
        var code = form.find('.code').val();
        var qty = form.find('.product_qty').val();
        formData.append("code", code);
        formData.append("qty", qty);
        //*********************//
        $.ajax({
            url: form.attr('action'),
            type: 'POST',
            data: formData,
            contentType: false,
            processData: false,
            beforeSend: function () {
                btn_update.attr('disabled', 'disabled');
            },
            success: function (data) { 
                
            },
            error: function (xhr) {

            },
            complete: function () {
                location.reload();
            }
        });
        //*********************//
        //restablecer url original
        form.attr('action', action_form);
    });

    $(".remove-btn").click(function () {
        var form = $(this).closest('form');
        var action_form = form.attr('action');
        var btn_remove = $(this);
        form.attr('action', form.attr('action') + "/removeItem");
        var formData = new FormData();
        var code = $(this).closest("form").find('.code').val();
        formData.append("code", code);
        //*********************//
        $.ajax({
            url: form.attr('action'),
            type: 'POST',
            data: formData,
            contentType: false,
            processData: false,
            beforeSend: function () {
                btn_remove.attr('disabled', 'disabled');
            },
            success: function (data) {                
            },
            error: function (xhr) {

            },
            complete: function () {
                location.reload();
            }
        });
        //*********************//
        //restablecer url original
        form.attr('action', action_form);
    });


})(jQuery);


