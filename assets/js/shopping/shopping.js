(function ($) {
    var _SUCCESS = false;
    var _REDIRECT = "";
    $("#shoppin-modal").on('hide.bs.modal', function () {
        if (_SUCCESS) {
            window.location.replace(_REDIRECT);
        }
    });


    $("#purchase-btn").click(function () {
        //
        var form = $("#form-pruchase");
        var formData = new FormData(document.getElementById("form-pruchase"));
        $.ajax({
            url: form.attr('action') + "/shopping/saveShopping",
            type: 'POST',
            data: formData,
            contentType: false,
            processData: false,
            beforeSend: function () {
                $("#purchase-btn").attr('disabled', 'disabled');
            },
            success: function (data) {
                try {
                    if (data !== "") {
                        var obj = jQuery.parseJSON(data);
                        if (obj.title !== undefined) {
                            _SUCCESS = true;
                            _REDIRECT = obj.url;
                            $("#shopping-title-msg").html(obj.title);
                            $("#shopping-msg-msg").html(obj.msg);
                            $('#shoppin-modal').modal('show');
                        }
                    }
                } catch (e) {

                }
                //window.location.replace(data);

            },
            error: function (xhr) {
                var obj = jQuery.parseJSON(xhr.responseText);
                //alert("Error: " + (jQuery.parseJSON(xhr.responseText).address));
                if (obj.customer !== undefined) {
                    $('#form-customer > div.invalid-feedback').html(obj.customer);
                    $('#form-customer > input').addClass('is-invalid');
                }
                if (obj.email !== undefined) {
                    $('#form-email > div.invalid-feedback').html(obj.email);
                    $('#form-email > input').addClass('is-invalid');
                }
                if (obj.address !== undefined) {
                    $('#form-address > div.invalid-feedback').html(obj.address);
                    $('#form-address > textarea').addClass('is-invalid');
                }
                $("#purchase-btn").removeAttr("disabled");
            },
            complete: function () {
                $("#purchase-btn :input").removeAttr("disabled");
                //location.reload();
            }
        });

    });

})(jQuery);
