function alertMessage(m) {

    $(".box-message").prepend(('<div class="alert alert-' + m.type + ' alert-dismissible fade show" role="alert">' +
            m.msg +
            '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
            '<span aria-hidden="true">&times;</span>' +
            '</button></div>'));
}