<?php
defined('BASEPATH') OR exit('No direct script access allowed');

function dashboard_template($page,$data = array()) {
    $ci = & get_instance();
    $ci->load->view('layouts/head', $data);
    $ci->load->view('layouts/bodynav');
    //**Página**//
    $ci->load->view("$page");
    //**********//
    
    $ci->load->view("layouts/footer"); 
}