<?php

/*
 * Description of cart_items_count_helper
 *
 * @author Juan C. Carrizo
 */

function cart_items_count() {
    $CI = & get_instance();
    $CART_COUNT = 0;
    //$CI->load->helper("session");
    if ($CI->session->tempdata('cart')) {
        $cart_items = $CI->session->tempdata('cart');
        $count = 0;
        foreach ($cart_items as $item) {
            $count += $item['product_qty'];
        }
        $CART_COUNT = $count;
    }
    return $CART_COUNT;
}

function getAllCartItems(){
    $CI = & get_instance();
    $cart_items = $CI->session->tempdata('cart');    
    $CI->load->model("Loadproductsmodelo");
    if($cart_items){
        return $CI->Loadproductsmodelo->getAllProductsDetailsItemsHelper($cart_items);   
    }else{
        return null;
    }     
}

function updateCartItem($id, $qty){
    $CI = & get_instance();
    $cart_items = $CI->session->tempdata('cart');
    foreach ($cart_items as $key => $item){
        if($item['product_id'] == $id){
            $cart_items[$key]['product_qty'] = $qty;
            $CI->session->set_tempdata('cart', $cart_items, 600);
            break;
        }
    }
    
}
