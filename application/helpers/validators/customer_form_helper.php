<?php
function getCustomerRules() {
    return array(
        array(
            'field' => 'customer',
            'label' => 'Nombre y apellido',
            'rules' => 'required|trim|min_length[3]|max_length[20]',
            'errors' => array(
                'required' => '¡El campo %s es requerido!',
                'min_length' => '¡El campo %s debe comprender entre 3 y 20 caracteres!',
                'max_length' => '¡El campo %s debe comprender entre 3 y 20 caracteres!',
            )
        ),
        array(
            'field' => 'email',
            'label' => 'Correo',
            'rules' => 'required|valid_email',
            'errors' => array(
                'required' => '¡El campo %s es requerido!',
                'min_length' => '¡Debe ingresar un %s valido!'
            )
        ),
        array(
            'field' => 'address',
            'label' => 'Dirección',
            'rules' => 'required|min_length[5]|max_length[255]',
            'errors' => array(
                'required' => '¡El campo %s es requerido!',
                'min_length' => '¡El campo %s debe comprender entre 5 y 255 caracteres!',
                'max_length' => '¡El campo %s debe comprender entre 5 y 255 caracteres!'
            )
        )
    );
}