<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Shoppingmodelo extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    public function saveShopping($customer_email, $customer_name, $address, $tax, $items) {
        $amount = 0.00;
        foreach ($items as $key => $item) {
            $amount += $item['total'];
        }
        $tabla = "shopping";
        $data = array(
            'customer_email' => $customer_email,
            'customer_name' => $customer_name,
            'date' => date("Y-m-d H:i:s"),
            'address' => $address,
            'amount' => $amount,
            'tax' => $tax,
        );
        $continue = false;
        $this->db->trans_start();
        $id = 0;
        if (!$this->db->insert($tabla, $data)) {
            return FALSE;
        } else {
            $tabla = "shopping_details";
            $id = $this->db->insert_id();
            $relation_data = array();
            
            foreach ($items as $key => $item) {
                array_push($relation_data, array("shopping_id"=>$id,"product_id" => $item['id'], "qty" => $item['qty'], "price" => $item['price'] ));
            }
            //shopping_id 	product_id 	qty 	price 
            foreach ($relation_data as $key => $item) {
                if (!$this->db->insert($tabla, $item)) {
                    return FALSE;
                } else {
                    $continue = true;
                }
            }
        }
        $this->db->trans_complete();
        return $continue;
    }

}
