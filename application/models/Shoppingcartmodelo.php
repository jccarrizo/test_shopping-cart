<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Shoppingcartmodelo extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    public function load_cart() {
        if ($this->session->tempdata('cart') != null) {
            $item_array_id = array_column($this->session->tempdata('cart'), 'product_id');
            $this->db->select('products.id,products.product_img, products.product_name, '
                    . 'products.product_description, '
                    . 'products.product_stock, products.product_price, categories.category_name');
            $this->db->from("products");
            $this->db->join('category_group', 'category_group.product_id = products.id');
            $this->db->join('categories', 'categories.id = category_group.category_id');
            $this->db->where_in("products.id", $item_array_id);
            $this->db->group_by("products.id");
            $this->db->where(array("products.product_status" => 1));

            $raw = $this->db->get();
            if ($raw->num_rows() === 0) {
                return false;
            }
            return $raw->result();
        }
    }

}
