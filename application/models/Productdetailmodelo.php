<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Productdetailmodelo extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    public function getProductDetails($id) {
        $this->db->select('products.id,products.product_img,'
                . ' products.product_name, products.product_description, '
                . 'products.product_stock, products.product_price,'
                . 'categories.category_name');
        $this->db->from("products");
        $this->db->join('category_group', 'category_group.product_id = products.id');
        $this->db->join('categories', 'categories.id = category_group.category_id');
        $this->db->where(array("products.product_status" => 1));
        $this->db->where(array("products.id"=>$id));

        $raw = $this->db->get();
        if ($raw->num_rows() === 0) {
            return null;
        }
        $results = $raw->result();
        $products = array();

        foreach ($results as $result) {
            $categories = array();
            $duplicate = false;
            if (empty($products)) {
                array_push($categories, $result->category_name);
            } else {
                foreach ($products as $key => $product) {
                    if ($products[$key]['id'] == $result->id) {
                        $categories = $products[$key]["categories"];
                        array_push($categories, $result->category_name);
                        $duplicate = true;
                        $products[$key]["categories"] = $categories;
                        break;
                    }
                }
                if (!$duplicate) {
                    array_push($categories, $result->category_name);
                }
            }
            if (!$duplicate) {
                $product = array(
                    "id" => $result->id,
                    "name" => $result->product_name,
                    "description" => $result->product_description,
                    "price" => $result->product_price,
                    "image" => $result->product_img,
                    "stock" => $result->product_stock,
                    "categories" => $categories
                );
                array_push($products, $product);
            }
        }
        return $products;
    }

}
