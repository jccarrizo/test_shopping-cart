<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Loadproductsmodelo extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    public function loadproduclist($filters = null) {
        $this->db->select('products.id,products.product_img, products.product_name, products.product_description, products.product_stock, products.product_price');
        $this->db->from("products");
        if ($filters !== null) {
            $this->db->join('category_group', 'category_group.product_id = products.id');
            $this->db->where(array("products.product_status" => 1));
            $this->db->where_in("category_group.category_id", $filters);
            $this->db->group_by("products.id");
        } else {
            $this->db->where(array("products.product_status" => 1));
            $this->db->limit(9, 0);
        }

        $this->db->order_by('id', 'RANDOM');
        $raw = $this->db->get();
        if ($raw->num_rows() === 0) {
            return false;
        }

        return $raw->result();
    }

    public function getAllProductsDetails($limit = null) {
        $this->db->select('products.id,products.product_img,'
                . ' products.product_name, products.product_description, '
                . 'products.product_stock, products.product_price,'
                . 'categories.category_name');
        $this->db->from("products");
        $this->db->join('category_group', 'category_group.product_id = products.id');
        $this->db->join('categories', 'categories.id = category_group.category_id');
        $this->db->where(array("products.product_status" => 1));
        if ($limit !== null) {
            $this->db->limit($limit, 0);
        }
        $raw = $this->db->get();
        if ($raw->num_rows() === 0) {
            return null;
        }
        $results = $raw->result();
        $products = array();

        foreach ($results as $result) {
            $categories = array();
            $duplicate = false;
            if (empty($products)) {
                array_push($categories, $result->category_name);
            } else {
                foreach ($products as $key => $product) {
                    if ($products[$key]['id'] == $result->id) {
                        $categories = $products[$key]["categories"];
                        array_push($categories, $result->category_name);
                        $duplicate = true;
                        $products[$key]["categories"] = $categories;
                        break;
                    }
                }
                if (!$duplicate) {
                    array_push($categories, $result->category_name);                    
                }
            }
            if (!$duplicate) {
                $product = array(
                    "id" => $result->id,
                    "name" => $result->product_name,
                    "description" => $result->product_description,
                    "price" => $result->product_price,
                    "image" => $result->product_img,
                    "stock" => $result->product_stock,
                    "categories" => $categories
                );
                array_push($products, $product);
            }
        }
        return $products;
    }
    
    
    
    
    public function getAllProductsDetailsItemsHelper($cart_items) {
        
        $item_array_id = array_column($cart_items, 'product_id');
        
        $this->db->select('products.id,products.product_img, '
                . 'products.product_name, products.product_description, '
                . 'products.product_stock, products.product_price,'
                . 'categories.category_name');
        $this->db->from("products");
        $this->db->join('category_group', 'category_group.product_id = products.id');
        $this->db->join('categories', 'categories.id = category_group.category_id');
        $this->db->where_in("products.id", $item_array_id);
        $this->db->where(array("products.product_status" => 1));
        
        $raw = $this->db->get();
        if ($raw->num_rows() === 0) {
            return null;
        }
        $results = $raw->result();
        $products = array();

        foreach ($results as $result) {
            $categories = array();
            $duplicate = false;
            if (empty($products)) {
                array_push($categories, $result->category_name);
            } else {
                foreach ($products as $key => $product) {
                    if ($products[$key]['id'] == $result->id) {
                        $categories = $products[$key]["categories"];
                        array_push($categories, $result->category_name);
                        $duplicate = true;
                        $products[$key]["categories"] = $categories;
                        break;
                    }
                }
                if (!$duplicate) {
                    array_push($categories, $result->category_name);                    
                }
            }
            if (!$duplicate) {
                $QTY = 0;
                foreach ($cart_items as $item){
                    if($result->id == $item['product_id']){
                        $QTY = $item['product_qty'];
                        break;
                    }
                }
                $product = array(
                    "id" => $result->id,
                    "name" => $result->product_name,
                    "description" => $result->product_description,
                    "price" => $result->product_price,
                    "image" => $result->product_img,
                    "stock" => $result->product_stock,
                    "categories" => $categories,
                    "qty" => $QTY,
                    "total" => $QTY*$result->product_price
                );
                array_push($products, $product);
            }
        }
        return $products;
    }

    public function loadCategories() {
        $this->db->select('*');
        $this->db->from("categories");
        //$this->db->join('paginas_contenido', 'paginas_contenido.id_pagina = paginas.id', 'left');
        //$this->db->where(array("products.product_status" => 1));
        //$this->db->limit(10,20);
        //$this->db->limit(9, 1);

        $this->db->order_by('category_name');
        $raw = $this->db->get();
        if ($raw->num_rows() === 0) {
            return false;
        }
        return $raw->result();
    }

}
