<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Shopping
 *
 * @author Juan C. Carrizo
 */
class Shopping extends CI_Controller {

    private $expire;
    private $tax = 0.00;

    public function __construct() {
        parent::__construct();
        $this->expire = 1800;
        $this->tax = 1.59;
        $this->load->library('form_validation');
        $this->load->helper(array("add_js_file","validators/customer_form"));//getCustomerRules
        $this->load->model("Shoppingmodelo");
    }

    public function index() {
        $data = array();
        $cart_items = $this->session->tempdata('cart');
        $this->session->set_tempdata('cart', $cart_items, 1800);
        $data["cart"] = getAllCartItems();
        $data['tax'] = $this->tax;
        
        add_javascript(array("js/shopping/shopping.js", "js/bootstrap-input-spinner.js"));
        dashboard_template("shopping/index", $data);
    }

    public function saveShopping() {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }
        $this->form_validation->set_rules(getCustomerRules());
        if ($this->form_validation->run() == FALSE) {
            $this->output->set_status_header(400);
            echo json_encode($this->form_validation->error_array());
            return;
        }
        
        
        
        $customer = $this->input->post('customer');
        $email = $this->input->post('email');
        $address = $this->input->post('address');
        $items = getAllCartItems();
        if ($items) {
            if ($this->Shoppingmodelo->saveShopping($email, $customer, $address, $this->tax, $items)) {
                $this->session->unset_tempdata('cart');
                $title = 'Hecho';
                $msg = '¡Gracias por su compra!';
                $url = base_url();
                $response = array('title' => $title, 'msg' => $msg, 'url' => $url );
                echo json_encode($response);
                //echo base_url();
            } else {
                
            }
        }

        //
        //$cart = getAllCartItems();
    }

}
