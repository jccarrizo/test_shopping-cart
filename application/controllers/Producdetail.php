<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Producdetail extends CI_Controller {
    
    private $expire = 600;

    public function __construct() {
        parent::__construct();
        $this->load->model("Productdetailmodelo");
    }

    public function index() {
        $data = array();
        //*************************//
        if ($this->input->post('code')) {
            $product_id = $this->input->post('code');
            $product_qty = $this->input->post('product_qty');

            if ($this->session->tempdata('cart') != null) {
                $temparray = $this->session->tempdata('cart');
                $item_array_id = array_column($this->session->tempdata('cart'), 'product_id');
                if (in_array($product_id, $item_array_id)) {
                    for ($i = 0; $i < count($temparray); $i++) {
                        if ($temparray[$i]['product_id'] == $product_id) {
                            $qty = $temparray[$i]['product_qty'];
                            $add_qty = $qty + $product_qty;
                            $temparray[$i]['product_qty'] = $add_qty;
                            $this->session->set_tempdata('cart', $temparray, $this->expire);
                            break;
                        }
                    }
                } else {
                    $aux_tempdata = array('product_id' => $product_id, 'product_qty' => $product_qty);
                    array_push($temparray, $aux_tempdata);
                    $this->session->set_tempdata('cart', $temparray, $this->expire);
                }
            } else {
                $tempdata = array(['product_id' => $product_id, 'product_qty' => $product_qty]);
                $this->session->set_tempdata('cart', $tempdata, $this->expire);
            }
        }
        //*************************//

        $id = $this->uri->segment(2);
        $data["product"] = $this->Productdetailmodelo->getProductDetails($id);
        $data["pid"] = $id;
        $this->load->helper("add_js_file");
        add_javascript(array("js/productdetail/productDetail.js", "js/bootstrap-input-spinner.js"));
        dashboard_template("productdetail/index", $data);
    }

}
