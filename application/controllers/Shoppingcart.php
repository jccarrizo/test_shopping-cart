<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Shoppingcart extends CI_Controller {

    private $expire;

    public function __construct() {
        parent::__construct();
        $this->load->model("Shoppingcartmodelo");
        $this->expire = 600;
    }

    public function index() {
        $data = array();

//        if(getAllCartItems())
//        print_r(getAllCartItems());
        $this->load->helper("add_js_file");
        add_javascript(array("js/shoppingcart/shoppingCart.js", "js/bootstrap-input-spinner.js"));
        //$products_id = array_column($this->session->tempdata('cart'), "product_id");
        $data["data_cart"] = getAllCartItems();
        //print_r($data["products_list"]);
        dashboard_template("cart/index", $data);
    }

    public function updateItem() {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }
        if ($this->input->post('code')) {
            $id = $this->input->post('code');
            $qty = $this->input->post('qty');
            updateCartItem($id, $qty);
        }
    }

    public function removeItem() {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }
        if ($this->input->post('code')) {
            $id = $this->input->post('code');
            $temp_sesion = $this->session->tempdata('cart');
            foreach ($temp_sesion as $key => $value) {
                if ($value["product_id"] == $id) {
                    unset($temp_sesion[$key]);
                    $reindex = array_values($temp_sesion); //normalize index
                    $temp_sesion = $reindex; //update variable
                    $this->session->set_tempdata('cart', $temp_sesion, $this->expire);
                    break;
                }
            }
        }
    }

}
