<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

    private $expire;

    public function __construct() {
        parent::__construct();
        $this->load->model("Loadproductsmodelo");
        //$this->load->library('session');
        $this->expire = 600;
    }

    public function index() {
        $data = array();
        if ($this->input->post('product_id')) {
            $product_id = $this->input->post('product_id');
            $product_qty = $this->input->post('product_qty');
            
            if ($this->session->tempdata('cart') != null) {
                $temparray = $this->session->tempdata('cart');
                $item_array_id = array_column($this->session->tempdata('cart'), 'product_id');
                if (in_array($product_id, $item_array_id)) {
                    for ($i = 0; $i < count($temparray); $i++) {
                        if ($temparray[$i]['product_id'] == $product_id) {
                            $qty = $temparray[$i]['product_qty'];
                            $add_qty = $qty + $product_qty;
                            $temparray[$i]['product_qty'] = $add_qty;
                            $this->session->set_tempdata('cart', $temparray, $this->expire);
                            break;
                        }
                    }
                } else {
                    $aux_tempdata = array('product_id' => $product_id, 'product_qty' => $product_qty);
                    array_push($temparray, $aux_tempdata);
                    $this->session->set_tempdata('cart', $temparray, $this->expire);
                }
            } else {
                $tempdata = array(['product_id' => $product_id, 'product_qty' => $product_qty]);
                $this->session->set_tempdata('cart', $tempdata, $this->expire);
            }
        }
        //$this->session->unset_tempdata('cart');
        if (($this->input->post('categories')) !== null) {
            $filters = $this->input->post('categories');
            $data["filters"] = $filters;
            $data["products"] = $this->Loadproductsmodelo->loadproduclist($filters);
        } else {
            $data["products"] = $this->Loadproductsmodelo->loadproduclist();
        }

        $data["categories"] = $this->Loadproductsmodelo->loadCategories();
        $this->load->helper("add_js_file");
        add_javascript(array("js/home/addToCart.js", "js/bootstrap-input-spinner.js"));
        dashboard_template("home/index", $data);
    }

}
