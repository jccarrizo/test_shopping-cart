<?php ?>

<div class="container-fluid" style="min-height: 1024px">

    <div class="row px-5">
        <div class="col-md-12">
            <div class="shopping-cart">
                <h6>Descripción del Producto</h6>
                <hr>
                <?php
                $total = 0.00;
                if ($product):
                    foreach ($product as $item):
                        ?>
                        <form id="product_detail" action="<?= base_url("product-detail")."/$pid"; ?>" method="POST" class="cart-items form-items" >

                            <div class="border shadow-sm p-3 mb-5 bg-white rounded" >
                                <div class="row bg-white">
                                    <div class="col-md-3">
                                        <img src="<?= assets("images/" . $item['image'] . "") ?>" class="img-fluid">
                                    </div>
                                    <div class="col-md-9">
                                        <h5 class="pt-2" ><?= $item['name'] ?></h5>
                                        <small class="text-secondary">Seller</small>                                        
                                        <h5 class="pt-2" >Precio: $ <?= number_format($item['price'], 2, ',', '.'); ?></h5>

                                        <div class="pr-2" style="float: left">
                                            <a href="<?= base_url(); ?>" class="btn btn-primary">Continuar comprando</a>
                                        </div>
                                        <div class="form-group row pr-2" style="width: 180px; float: left">                                        
                                            <div class="col-12">
                                                <input type="number" value="1" min="1" max="<?= $item['stock'] ?>" step="1" name="product_qty"/>
                                                <input type="hidden" value="<?= $item['id']  ?>" name="code" class="code">
                                            </div>
                                        </div>

                                        <button type="submit" class="btn btn-warning addcart-btn">Agregar al carrito <i class="fa fa-shopping-cart"></i></button>


                                    </div>

                                    <div class="col-md-12">
                                        <h5>Detalles</h5>
                                        <small class="text-secondary"><?= $item['description'] ?></small>
                                    </div>


                                </div>
                            </div>
                        </form>
                        <?php
                    endforeach;
                else:
                    echo '<h6>¡Este producto no existe!</h6>';
                endif;
                ?>
            </div>
        </div>
    </div>
</div>