<?php ?>

<!-- Page Content -->
<div class="container-fluid">

    <div class="row">

        <div class="col-lg-3">

            <h1 class="my-4">Shop Name</h1>
            <form name="category_filter" action="<?php base_url() ?>" method="POST">
                <div class="list-group">
                    <?php
                    foreach ($categories as $category) {
                        $chck = "";
                        if (isset($filters)) {
                            foreach ($filters as $filter) {
                                if ($category->id === $filter) {
                                    $chck = "checked";
                                }
                            }
                        }
                        ?>
                        <div class="list-group-item"><label for="cat<?= $category->id ?>"><?= $category->category_name ?></label> <input id="cat<?= $category->id ?>" <?= $chck ?>  type="checkbox" name="categories[]" value="<?= $category->id ?>"></div>
                    <?php } ?>
                    <button class="btn btn-primary" type="submit">Aplicar filtro</button>
                </div>

            </form>
        </div>
        <!-- /.col-lg-3 -->

        <div class="col-lg-9">

            <div id="carouselExampleIndicators" class="carousel slide my-4" data-ride="carousel">
                <ol class="carousel-indicators">
                    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                </ol>
                <div class="carousel-inner" role="listbox">
                    <div class="carousel-item active">
                        <img class="d-block img-fluid" src="<?= assets('images/banners/celularesbanner.jpg') ?>" alt="First slide">
                    </div>
                    <div class="carousel-item">
                        <img class="d-block img-fluid" src="<?= assets('images/banners/banner-img-darken-1920x850.jpg') ?>" alt="Second slide">
                    </div>
                    <div class="carousel-item">
                        <img class="d-block img-fluid" src="<?= assets('images/banners/camarabanner.jpg') ?>" alt="Third slide">
                    </div>
                </div>
                <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>

            <div class="row">

                <?php if (($products) != null) { ?>

                    <?php foreach ($products as $product) {
                        ?>
                        <form action="<?php echo base_url(); ?>" method="POST" class="col-lg-4 col-md-6 mb-4">
                            <div class="card h-100">
                                <a href="<?= base_url('product-detail/').$product->id ?>"><img class="card-img-top" src="<?= assets('images/' . $product->product_img) ?>" onerror="this.src = 'http://placehold.it/700x400'"  alt=""></a>
                                <div class="card-body">
                                    <h4 class="card-title">
                                        <a href="<?= base_url('product-detail/').$product->id ?>"><?= $product->product_name ?></a>
                                    </h4>
                                    <span class="price">Precio: $ <?= number_format($product->product_price, 2, ',', '.'); ?></span>
                                    <div class="overflow-hidden"  style="max-height: 262px">
                                        <div class="card-text"><b>Descripción:</b></div>
                                        <p class="card-text"><?= $product->product_description ?></p>
                                    </div>

                                </div>
                                <div class="card-footer">
                                    <div class="form-group row">                                        
                                        <div class="col-10">
                                            <input type="number" value="1" min="1" max="<?= $product->product_stock ?>" step="1" name="product_qty"/>
                                        </div>
                                    </div>
                                    <button class="btn btn-warning" type="submit" onclick="addToCart(<?= $product->id ?>)" >Add to Cart <i class="fa fa-shopping-cart"></i></button>
                                    <a href="<?= base_url("product-detail/" . $product->id); ?>" style="margin-top: 10px; margin-bottom: 10px" class="btn btn-primary" >Detalles</a>
                                    <input type="hidden" value="<?= $product->id ?>" name="product_id">
                                    <!--small class="text-muted">&#9733; &#9733; &#9733; &#9733; &#9734;</small-->
                                </div>
                            </div>
                        </form>
                    <?php } ?>

                <?php } else { ?>
                    <div class="col-lg-4 col-md-6 mb-4" style="height: 250px">                    
                        No hay registros para esta categoría
                    </div>
                <?php } ?>

                <!--div class="col-lg-4 col-md-6 mb-4">
                  <div class="card h-100">
                    <a href="#"><img class="card-img-top" src="http://placehold.it/700x400" alt=""></a>
                    <div class="card-body">
                      <h4 class="card-title">
                        <a href="#">Item Two</a>
                      </h4>
                      <h5>$24.99</h5>
                      <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet numquam aspernatur! Lorem ipsum dolor sit amet.</p>
                    </div>
                    <div class="card-footer">
                      <small class="text-muted">&#9733; &#9733; &#9733; &#9733; &#9734;</small>
                    </div>
                  </div>
                </div>
      
                <div class="col-lg-4 col-md-6 mb-4">
                  <div class="card h-100">
                    <a href="#"><img class="card-img-top" src="http://placehold.it/700x400" alt=""></a>
                    <div class="card-body">
                      <h4 class="card-title">
                        <a href="#">Item Three</a>
                      </h4>
                      <h5>$24.99</h5>
                      <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet numquam aspernatur!</p>
                    </div>
                    <div class="card-footer">
                      <small class="text-muted">&#9733; &#9733; &#9733; &#9733; &#9734;</small>
                    </div>
                  </div>
                </div>
      
                <div class="col-lg-4 col-md-6 mb-4">
                  <div class="card h-100">
                    <a href="#"><img class="card-img-top" src="http://placehold.it/700x400" alt=""></a>
                    <div class="card-body">
                      <h4 class="card-title">
                        <a href="#">Item Four</a>
                      </h4>
                      <h5>$24.99</h5>
                      <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet numquam aspernatur!</p>
                    </div>
                    <div class="card-footer">
                      <small class="text-muted">&#9733; &#9733; &#9733; &#9733; &#9734;</small>
                    </div>
                  </div>
                </div>
      
                <div class="col-lg-4 col-md-6 mb-4">
                  <div class="card h-100">
                    <a href="#"><img class="card-img-top" src="http://placehold.it/700x400" alt=""></a>
                    <div class="card-body">
                      <h4 class="card-title">
                        <a href="#">Item Five</a>
                      </h4>
                      <h5>$24.99</h5>
                      <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet numquam aspernatur! Lorem ipsum dolor sit amet.</p>
                    </div>
                    <div class="card-footer">
                      <small class="text-muted">&#9733; &#9733; &#9733; &#9733; &#9734;</small>
                    </div>
                  </div>
                </div>
      
                <div class="col-lg-4 col-md-6 mb-4">
                  <div class="card h-100">
                    <a href="#"><img class="card-img-top" src="http://placehold.it/700x400" alt=""></a>
                    <div class="card-body">
                      <h4 class="card-title">
                        <a href="#">Item Six</a>
                      </h4>
                      <h5>$24.99</h5>
                      <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet numquam aspernatur!</p>
                    </div>
                    <div class="card-footer">
                      <small class="text-muted">&#9733; &#9733; &#9733; &#9733; &#9734;</small>
                    </div>
                  </div-->
            </div>

        </div>
        <!-- /.row -->

    </div>
    <!-- /.col-lg-9 -->

</div>
<!-- /.row -->

</div>
<!-- /.container -->
