<?php ?>

<div class="container-fluid" style="min-height: 100%">
    <div class="row px-5">
        <div class="col-md-8">
            <div class="shopping-cart">
                <h6>My Cart</h6>
                <hr>
                <?php
                $total = 0.00;
                if ($data_cart):
                    foreach ($data_cart as $item):
                        ?>
                        <form action="<?= base_url("cart"); ?>" method="POST" class="cart-items form-items" >
                            <div class="border shadow-sm p-3 mb-5 bg-white rounded" >
                                <div class="row bg-white">
                                    <div class="col-md-3">
                                        <img src="<?= assets("images/" . $item['image'] . "") ?>" class="img-fluid">
                                    </div>
                                    <div class="col-md-9">
                                        <h5 class="pt-2" ><?= $item['name'] ?></h5>
                                        <small class="text-secondary">Seller</small>                                        
                                        <h5 class="pt-2" >Precio unitario: $ <?= number_format($item['price'], 2, ',', '.'); ?></h5>
                                        <h5 class="pt-2" >Precio por <?= $item['qty'] ?> unidad(es) : $ <?= number_format($item['price'] * $item['qty'], 2, ',', '.'); ?></h5>
                                        <div class="pr-2" style="float: left">
                                            <a href="<?= base_url(); ?>" class="btn btn-warning">Continuar comprando</a>
                                        </div>
                                        <div class="pr-2" style="width: 150px; float: left">
                                            <input type="number" value="<?= $item['qty']; ?>" min="1" max="100" step="1" name="product_qty" class="form-control w-25 product_qty"/>
                                        </div>
                                        <button class="btn btn-success update-btn">Actualizar</button>

                                        <div class="py-2" style="width: 150px;">
                                            <div>                                                  
                                                <button class="btn btn-danger mt-2 remove-btn" name="remove">Quitar producto</button>
                                                <input type="hidden" value="<?= $item['id'] ?>" name="code" class="code">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <h5>Detalles</h5>
                                        <small class="text-secondary"><?= $item['description'] ?></small>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <?php
                    endforeach;
                else:
                    echo '<h6>Cart is Empty</h6>';
                endif;
                ?>
            </div>
        </div>
        <div class="col-md-4 border rounded mt-5 bg-white h-25">
            <div class="pt-4">
                <h6>Price Details</h6>
                <hr>
                <div class="row price-details">
                    <div class="col-md-6">

                        <?php
                        $tp = 0;
                        if ($data_cart) {
                            foreach ($data_cart as $cart_item) {
                                $tp += $cart_item["qty"];
                                $total += $cart_item["total"];
                            }
                        }
                        $cargo = 1.59;
                        ?>
                        <h6>Productos: <?= $tp; ?></h6>                        
                    </div>
                    <div class="col-md-6">
                        Sub Total: $<?= number_format(($total), 2, ',', '.'); ?>
                    </div>
                    <div class="col-md-6">
                        Cargos:
                        <hr>
                    </div>
                    <div class="col-md-6">
                        $ <?= number_format($cargo, 2, ',', '.'); ?>
                        <hr>
                    </div>
                    <div class="col-md-6 mb-4">Total a pagar:</div>
                    <div class="col-md-6 mb-4">$<?= number_format($total + $cargo, 2, ',', '.'); ?></div>
                    <?php if ($data_cart): ?>
                        <div class="col-md-12">
                            <hr>
                            <p><b>Ofertar</b></p>
                            <div class="col-md-6 mb-4"><a href="<?= base_url("shopping"); ?>" class="btn btn-success">Proceder a Pagar</a></div>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>