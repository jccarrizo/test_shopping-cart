<?php
$products_coutn = 0;
$sub_total = 0.00;
if ($cart) {
    foreach ($cart as $key => $value) {
        $products_coutn += $value['qty'];
        $sub_total += $value['total'];
    }
}
?>

<div class="container-fluid" style="min-height: 680px">
    <div class="row px-5">
        <div class="col-md-8">
            <div class="shopping-cart">
                <h6>Pago en un solo paso</h6>
                <hr>
                <div class="border shadow-sm p-3 mb-5 bg-white rounded" >
                    <?php if ($cart) { ?>
                        <form id="form-pruchase" action="<?= base_url(); ?>" method="POST">
                            <div class="container">
                                <div class="row">
                                    <div class="col">
                                        <div id="form-customer" class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="basic-addon1">@</span>
                                            </div>
                                            <input type="text" class="form-control" placeholder="Nombre y apellido" name="customer" aria-label="Username" aria-describedby="basic-addon1">
                                            <div class="invalid-feedback d-block">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div id="form-email" class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="basic-addon2">@</span>
                                            </div>
                                            <input name="email" type="email" class="form-control" placeholder="Correo" aria-label="Email" aria-describedby="basic-addon2">
                                            <div class="invalid-feedback d-block">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="w-100"></div>
                                    <div class="col">
                                        <div id="form-address" class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text">Dirección</span>
                                            </div>
                                            <textarea name="address" class="form-control" aria-label="Dirección"></textarea>
                                            <div class="invalid-feedback d-block">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col"></div>
                                </div>
                            </div>
                        </form>
                        <?php
                    } else {
                        echo '<h6>Cart is Empty</h6>';
                    }
                    ?>
                </div>
            </div>
        </div>
        <?php if ($cart) { ?>
            <div class="col-md-4">
                <div class="col-md-12 border rounded mt-5 bg-white">
                    <div class="pt-4">
                        <h6>Price Details</h6>
                        <hr>
                        <div class="row price-details">
                            <div class="col-md-6">
                                <h6>Productos: <?= $products_coutn ?></h6>                        
                            </div>
                            <div class="col-md-6">
                                Sub Total: $ <?= number_format($sub_total, 2, ',', '.'); ?>
                            </div>
                            <div class="col-md-6">
                                Inpuestos:
                                <hr>                            
                            </div>
                            <div class="col-md-6">
                                $ <?= number_format(($tax), 2, ',', '.'); ?>
                                <hr>
                            </div>
                            <div class="col-md-6 mb-4">Total a pagar:</div>
                            <div class="col-md-6 mb-4">$ <?= number_format(($sub_total + $tax), 2, ',', '.'); ?></div>
                            <div class="col-md-6">
                                <button class="btn btn-success mb-3" id="purchase-btn">Proceder</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        <?php } ?>
    </div>

</div>
<!-- Modal -->
<div class="modal fade" id="shoppin-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="shopping-title-msg"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times</span>
                </button>
            </div>
            <div id="shopping-msg-msg" class="modal-body">
                ...
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

